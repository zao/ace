#include "stb_image.c"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#include <string>
#include <iostream>
#include <bitset>

struct img {
    char r;
    char g;
    char b;
    char a;
};

bool clubs[] =
{0,0,0,0,1,0,0,0,0,
 0,0,0,1,1,1,0,0,0,
 0,0,0,0,1,0,0,0,0,
 0,0,1,0,1,0,1,0,0,
 0,1,1,1,1,1,1,1,0,
 0,0,1,0,1,0,1,0,0,
 0,0,0,0,1,0,0,0,0,
 0,0,0,0,1,0,0,0,0,
 0,0,0,1,1,1,0,0,0};

bool diamonds[] =
{0,0,0,0,1,0,0,0,0,
 0,0,0,0,0,0,0,0,0,
 0,0,0,1,0,1,0,0,0,
 0,0,0,0,0,0,0,0,0,
 0,0,1,0,0,0,1,0,0,
 0,0,0,0,0,0,0,0,0,
 0,0,0,1,0,1,0,0,0,
 0,0,0,0,0,0,0,0,0,
 0,0,0,0,1,0,0,0,0};

bool hearts[] =
{0,0,0,0,0,0,0,0,0,
 0,0,1,0,0,0,1,0,0,
 0,1,0,1,0,1,0,1,0,
 0,1,0,0,1,0,0,1,0,
 0,1,0,0,0,0,0,1,0,
 0,0,1,0,0,0,1,0,0,
 0,0,0,1,0,1,0,0,0,
 0,0,0,0,1,0,0,0,0,
 0,0,0,0,0,0,0,0,0};

bool spades[] =
{0,0,0,0,0,0,0,0,0,
 0,0,0,0,1,0,0,0,0,
 0,0,0,1,0,1,0,0,0,
 0,0,1,0,0,0,1,0,0,
 0,1,0,0,1,0,0,1,0,
 0,1,0,1,1,1,0,1,0,
 0,1,1,0,1,0,1,1,0,
 0,0,0,0,1,0,0,0,0,
 0,0,0,1,1,1,0,0,0};

unsigned char *data;
int x, y, comp;

void fun(const bool *face, const std::string filename, const bool red) {
    size_t ix = 0;
    size_t iy = 0;
    for(size_t i=0; i<x*y*comp; i+=comp) {
	struct img x;
	x.r = data[i+0];
	x.g = data[i+1];
	x.b = data[i+2];
	x.a = data[i+3];

	if(face[(ix/14)+(iy/14)*9]) {
	    x.r = x.g = x.b = 0;
	    if (red) x.r = 255;
	    x.a = 255;
	} else {
	    x.r = x.g = x.b = 0;
	    x.a = 0;
	}

	ix++;
	if (ix%128 == 0) {
	    ix = 0;
	    iy++;
	}

	data[i+0] = x.r;
	data[i+1] = x.g;
	data[i+2] = x.b;
	data[i+3] = x.a;
    }

    stbi_write_png(filename.c_str(), x, y, comp, data, 1024);
}

int main() {
    const std::string filename = "cardH1.png";
    FILE *file = fopen(filename.c_str(), "rb");
    if (!file) return 0;
    data = stbi_load_from_file(file, &x, &y, &comp, 0);
    fclose(file);

    fun(clubs,	  "clubs.png",   false);
    fun(spades,   "spades.png",  false);
    fun(hearts,   "hearts.png",   true);
    fun(diamonds, "diamonds.png", true);

    free(data);
}
