cmake_minimum_required(VERSION 2.8)

include_directories(
#	"include"
	"src"
)

set(CARDGEN_SOURCES
	"src/CardGen.cc"
)

add_executable(cardgen
	${CARDGEN_SOURCES}
)

target_link_libraries(cardgen
)
