#include "AceMesh.h"

#include <MathGeoLib.h>
#include <fstream>
#include <map>
#include <string>
#include <tuple>
#include <vector>

#include <tpl.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

struct MeshConverter
{
	explicit MeshConverter(std::string filename) {
		if (!ParseObj(filename))
			return;
		auto extPos = filename.find_last_of('.');
		std::string target = filename.substr(0, extPos) + ".acemesh";
		WriteMesh(target);
	}

private:
	typedef std::tuple<int, int, int> IndexTriple;

	typedef acemesh::Vertex Vertex;
	typedef acemesh::Index Index;

	bool ParseObj(std::string filename) {
		vs.clear();
		ts.clear();
		ns.clear();
		vs.push_back(float3::zero);
		ts.push_back(float3::zero);
		ns.push_back(float3::zero);
		dedup.clear();
		vertices.clear();
		indices.clear();

		std::ifstream is(filename.c_str());
		if (!is)
			return false;
		std::string line;
		while (std::getline(is, line)) {
			line = line.substr(0, line.find_first_of('#'));
			if(line.empty())
				continue;
			std::istringstream lineStream(line);
			std::string command;
			if (!(lineStream >> command))
				continue;
			if (command == "v") {
				ParseVertex(lineStream);
			}
			else if (command == "vt") {
				ParseTexcoord(lineStream);
			}
			else if (command == "vn") {
				ParseNormal(lineStream);
			}
			else if (command == "f") {
				ParseFace(lineStream);
			}
		}
		return true;
	}

	void WriteMesh(std::string filename) {
		acemesh::Header const header = { sizeof(Index), sizeof(Vertex) };
		acemesh::GeometryInfo geometryInfo;
		acemesh::StreamInfo streamInfo;
		tpl_bin const indicesBin = { indices.data(), (uint32_t)(indices.size() * sizeof(Index)) };
		tpl_bin const verticesBin = { vertices.data(), (uint32_t)(vertices.size() * sizeof(Vertex)) };
		auto tn = tpl_map(acemesh::FormatString(), &header,
						  &streamInfo,
						  &geometryInfo,
						  &indicesBin, &verticesBin);
		tpl_pack(tn, 0);

		acemesh::StreamInfo streamInfos[] = {
			{ 0, 12, "POSITION" },
			{ 12, 12, "TEXCOORD" },
			{ 24, 12, "NORMAL" }
		};

		for (auto si : streamInfos) {
			streamInfo = si;
			tpl_pack(tn, 1);
		}

		geometryInfo.offset = 0;
		geometryInfo.count = indices.size();
		tpl_pack(tn, 2);

		tpl_dump(tn, TPL_FILE, filename.c_str());
		tpl_free(tn);
	}

	void ParseVertex(std::istringstream& line) {
		float4 v(float3::zero, 1.0f);
		for (int i = 0; i < 4; ++i) {
			line >> v[i];
		}
		vs.push_back((v / v.w).xyz());
	}

	void ParseTexcoord(std::istringstream& line) {
		float3 t(float3::zero);
		for (int i = 0; i < 3; ++i) {
			line >> t[i];
		}
		t[1] = 1.0f - t[1];
		ts.push_back(t);
	}

	void ParseNormal(std::istringstream& line) {
		float3 n(float3::zero);
		for (int i = 0; i < 3; ++i) {
			line >> n[i];
		}
		ns.push_back(n.Normalized());
	}

	IndexTriple ParseTriple(std::string word) {
		IndexTriple ret{ 0, 0, 0 };
		std::istringstream is(word);
		auto nextPart = [&]{
			try {
				std::string part;
				std::getline(is, part, '/');
				return std::stoi(part);
			}
			catch (...) { return 0; }
		};
		std::get<0>(ret) = nextPart();
		std::get<1>(ret) = nextPart();
		std::get<2>(ret) = nextPart();
		return ret;
	}

	void ParseFace(std::istringstream& line) {
		int nodes = 0;
		Index face[3];
		std::string word;
		while (line >> word) {
			auto triple = ParseTriple(word);
			Index index = vertices.size();
			auto I = dedup.find(triple);
			if (I != dedup.end()) {
				index = I->second;
			}
			else {
				dedup[triple] = index;
				Vertex vertex = {
					Lookup(vs, std::get<0>(triple)),
					Lookup(ts, std::get<1>(triple)),
					Lookup(ns, std::get<2>(triple))
				};
				vertices.push_back(vertex);
			}
			switch (nodes) {
			case 0:
				face[0] = index;
				break;
			case 1:
				face[1] = index;
				break;
			case 2:
				face[2] = index;
				break;
			default:
				face[1] = face[2];
				face[2] = index;
			}
			if (nodes >= 2) {
				indices.insert(indices.end(), &face[0], &face[0] + 3);
			}
			++nodes;
		}
	}

	template <typename C>
	typename C::reference Lookup(C& c, int index) {
		if (index < 0) index = c.size() + index;
		return c.at(index);
	}

	template <typename C>
	typename C::const_reference Lookup(C const& c, int index) const {
		if (index < 0) index = c.size() + index;
		return c.at(index);
	}

	std::vector<float3> vs;
	std::vector<float3> ts;
	std::vector<float3> ns;

	std::map<IndexTriple, Index> dedup;

	std::vector<Vertex> vertices;
	std::vector<Index> indices;
};

void ConvertObjToMesh(std::string filename) {
	MeshConverter{filename};
}

int main(int argc, char** argv) {
	while (*++argv) {
		ConvertObjToMesh(*argv);
	}
}
