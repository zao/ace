#version 150

uniform mat4 in_world;
uniform mat4 in_view;
uniform mat4 in_projection;

in vec3 in_position;
in vec3 in_normal;
in vec3 in_texcoord;

out Frag {
	vec3 position;
	vec3 normal;
	vec3 texcoord;
	vec3 worldNormal;
} Out;

void main() {
	vec4 viewPos = in_view * in_world * vec4(in_position, 1.0);
	Out.position = viewPos.xyz / viewPos.w;
	gl_Position = in_projection * viewPos;
	Out.worldNormal = in_normal;
	Out.normal = normalize(transpose(inverse(mat3(in_view * in_world))) * in_normal);
	Out.texcoord = in_texcoord;
}
