#version 150

uniform vec3 in_lightPos;
uniform sampler2D in_surface;
uniform sampler2D in_glyph;

in Frag {
	vec3 position;
	vec3 normal;
	vec3 texcoord;
	vec3 worldNormal;
} In;

out vec4 out_fragColor;

void main() {
	vec3 n = normalize(In.normal);
	vec3 l = in_lightPos - In.position;
	float d = length(l);
	l = l/d;
	float Id = clamp(dot(n, l), 0.0, 1.0);
	vec3 v = normalize(-In.position);
	vec3 r = reflect(l, n);
	vec3 h = (l+v)/length(l+v);
	float Is = pow(clamp(dot(n, h), 0.0, 1.0), 8);
	vec3 Ks = vec3(0.3);
	
	vec2 tcSurface = In.texcoord.xy;
	vec2 tcGlyph = In.texcoord.xy;

	vec3 surface = texture(in_surface, tcSurface).rgb;
	vec4 glyph = In.worldNormal.y >= 0.0
		? texture(in_glyph, tcGlyph)
		: vec4(0.0);
	vec3 Ia = vec3(0.1);
	vec3 Kd = vec3(0.8) * mix(surface, glyph.rgb, glyph.a);
	float R = 100.0;
	float Fatt = 1.0/(1 + (2*d)/R + (d*d)/(R*R));

	vec3 c = Ia + Fatt*(Kd*Id + Ks*Is);
	out_fragColor = vec4(c, 1.0);
}
