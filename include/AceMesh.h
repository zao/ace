#pragma once
#include <cstdint>
#include <MathGeoLib.h>

namespace acemesh {
	struct Vertex {
		float3 v;
		float3 t;
		float3 n;
	};

	typedef uint32_t Index;

	struct Header {
		uint16_t indexStride;
		uint16_t vertexStride;
	};
	struct StreamInfo {
		uint32_t offset, size;
		char const* semantic;
	};
	struct GeometryInfo {
		uint32_t offset, count;
	};

	char* FormatString() {
		static char fmt[] = "S(vv)A(S(uus))A(S(uu))BB";
		return fmt;
	}
}
