// vim: set ts=4 sw=4 noet cin :
#pragma once

#include <cstdint>
#include <deque>
#include <vector>

namespace ace
{
	struct Noncopyable {
		Noncopyable() {}
	private:
		Noncopyable(Noncopyable const&) = delete;
		Noncopyable& operator = (Noncopyable const&) = delete;
	};

	struct Card {
		enum Suit { Hearts, Diamonds, Spades, Clubs };
		typedef uint8_t Value;
		Suit suit;
		Value value;
	};

	struct LexicalCardComparer {
		bool operator () (Card a, Card b) const {
			if (a.suit != b.suit)
				return a.suit < b.suit;
			return a.value < b.value;
		}
	};

	struct Deck {
		Deck();

		bool Empty() const { return cards.empty(); }
		int RemainingCount() const { return cards.size(); }
		Card DrawCard();

	private:
		std::deque<Card> cards;
	};

	struct Board {
		std::vector<Card> lanes[4];
	};

	struct Game : Noncopyable
	{
		Game();
		~Game();

		int CardCountInDeck() const;
		int CardCountInLane(int lane) const;
		int CardCountInPile() const;

		Card CardInLane(int lane, int card) const;
		Card CardInPile(int card) const;

		bool CanDealCards() const;
		bool CanDiscardCard(int lane) const;
		bool CanMoveCard(int laneFrom, int laneTo) const;

		void DealCards();
		void MoveCard(int laneFrom, int laneTo);
		bool DiscardCard(int lane);

	private:
		Deck deck;
		Board board;
		std::vector<Card> pile;
	};
}
