// vim: set ts=4 sw=4 noet cin :
#include "AceGame.h"

#include <algorithm>
#include <cassert>
#include <deque>

namespace ace
{
	struct ArbitraryDeckOrdering {
		bool operator () (Card a, Card b) const {
			if (a.suit != b.suit)
				return a.suit < b.suit;
			return a.value < b.value;
		}
	};

	static bool operator == (Card a, Card b) {
		return a.suit == b.suit && a.value == b.value;
	}

	static bool Outranked(Card candidate, Card reference) {
		if (candidate.suit != reference.suit || candidate == reference)
			return false;
		if (candidate.value == 1)
			return false;
		if (reference.value == 1)
			return true;
		return candidate.value < reference.value;
	}

	Deck::Deck() {
		for (int i = 1; i <= 13; ++i) {
			for (int suitIndex = 0; suitIndex < 4; ++suitIndex) {
				Card c;
				c.suit = (Card::Suit)suitIndex;
				c.value = (Card::Value)i;
				cards.push_back(std::move(c));
			}
		}
		std::random_shuffle(cards.begin(), cards.end());
	}

	Card Deck::DrawCard() {
		assert(!cards.empty());
		Card ret = cards.front();
		cards.pop_front();
		return ret;
	}


	Game::Game() {
	}

	Game::~Game() {
	}

	int Game::CardCountInDeck() const {
		return deck.RemainingCount();
	}

	int Game::CardCountInLane(int lane) const {
		return board.lanes[lane].size();
	}

	int Game::CardCountInPile() const {
		return pile.size();
	}

	Card Game::CardInLane(int lane, int card) const {
		return board.lanes[lane].at(card);
	}

	Card Game::CardInPile(int card) const {
		return pile.at(card);
	}

	bool Game::CanDealCards() const {
		return !deck.Empty();
	}

	static Card TopCard(Game const& game, int lane) {
		auto count = game.CardCountInLane(lane);
		return game.CardInLane(lane, count-1);
	}

	bool Game::CanDiscardCard(int lane) const {
		if (board.lanes[lane].size()) {
			Card candidate = TopCard(*this, lane);
			for (int i = 0; i < 4; ++i) {
				if (i != lane && board.lanes[i].size()) {
					Card reference = TopCard(*this, i);
					if (Outranked(candidate, reference))
						return true;
				}
			}
		}
		return false;
	}

	bool Game::CanMoveCard(int laneFrom, int laneTo) const {
		return board.lanes[laneFrom].size() && board.lanes[laneTo].empty();
	}

	void Game::DealCards() {
		if (!CanDealCards())
			return;
		for (int i = 0; i < 4; ++i) {
			Card card = deck.DrawCard();
			board.lanes[i].push_back(card);
		}
	}

	void Game::MoveCard(int laneFrom, int laneTo) {
		if (!CanMoveCard(laneFrom, laneTo))
			return;
		Card card = board.lanes[laneFrom].back();
		board.lanes[laneFrom].pop_back();
		board.lanes[laneTo].push_back(card);
	}

	bool Game::DiscardCard(int lane) {
		if (!CanDiscardCard(lane))
			return false;
		Card card = board.lanes[lane].back();
		board.lanes[lane].pop_back();
		pile.push_back(card);
		return true;
	}
}
