// vim: set ts=4 sw=4 noet cin :
#include "AceGame.h"
#include "AceMesh.h"

//#include <GL/glew.h>
#include <GLXW/glxw.h>
#include <GLFW/glfw3.h>
#include <GLZ/glz.h>

#include <MathGeoLib.h>
#define STBI_HEADER_FILE_ONLY
#include "stb_image.c"

#include <fstream>
#include <random>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <tpl.h>

namespace ace {
	struct GameController {
		void ActivateLane();
		void ActicateDeck();
	};
}

struct Camera {
	float4x4 view;
	float4x4 projection;
};

float laneXSeparation = 13.0f;
float laneDepthBase = -18.0f;
float laneDepthSeparation = 3.0f;

std::mt19937 laneRotationRng(9001);
std::uniform_real_distribution<float> laneRotationDist(-0.1f, 0.1f);
std::vector<float> laneRotations[4];

auto laneX = []{
	std::array<float, 4> ret;
	for (int lane = 0; lane < 4; ++lane) {
		ret[lane] = (lane-1.5f) * laneXSeparation;
	}
	return ret;
}();

float LaneDepth(int lane, int slot) {
	return laneDepthBase + laneDepthSeparation * slot;
}

float3x4 LaneLocation(int lane, int slot) {
	while ((size_t)slot >= laneRotations[0].size()) {
		laneRotations[0].push_back(laneRotationDist(laneRotationRng));
		laneRotations[1].push_back(laneRotationDist(laneRotationRng));
		laneRotations[2].push_back(laneRotationDist(laneRotationRng));
		laneRotations[3].push_back(laneRotationDist(laneRotationRng));
	}
	float laneDepth = LaneDepth(lane, slot);
	return
			float3x4::Translate(laneX[lane], 0.0f, laneDepth) *
			float3x4::RotateX(0.08f) *
			float3x4::RotateY(laneRotations[lane].at(slot));
}

OBB LaneBounds(int lane) {
	OBB ret;
	float midDepth = (LaneDepth(lane, 0) + LaneDepth(lane, 12)) / 2.0f;
	ret.pos = float3(laneX[lane], 0.0f, midDepth);
	ret.axis[0] = float3::unitX;
	ret.axis[1] = float3::unitY;
	ret.axis[2] = float3::unitZ;
	ret.r = float3(laneXSeparation/2.0f, 1.0f, laneDepthSeparation*13 / 2.0f);
	return ret;
}

float3 deckBase = float3(-30.0f, 5.0f, -20.0f);
float deckStackSeparation = 0.35f;
std::mt19937 deckShiftRng(9001);
std::uniform_real_distribution<float> deckShiftDist(-0.3f, 0.3f);
std::vector<float> deckShifts;

float3x4 DeckLocation(int index) {
	while ((size_t)index >= deckShifts.size()) {
		deckShifts.push_back(deckShiftDist(deckShiftRng));
	}
	float3 pos = deckBase;
	pos.x += deckShifts.at(index);
	pos.z += deckStackSeparation * index;
	return
			float3x4::Translate(pos) *
			float3x4::RotateX(pi/2) *
			float3x4::RotateZ(pi);
}

float3 pileBase = float3(32.0f, 0.0f, 0.0f);
float pileStackSeparation = 0.35f;
std::mt19937 pileStackRng(9001);
std::uniform_real_distribution<float> pileStackRotationDist(-0.5f, 0.5f);
std::vector<float> pileStackRotations;

float3x4 PileLocation(int index) {
	while (pileStackRotations.size() <= (size_t)index) {
		float angle = pileStackRotationDist(pileStackRng);
		pileStackRotations.push_back(angle);
	}
	float3 pos = pileBase;
	pos.y += index*pileStackSeparation;
	return
			float3x4::Translate(pos) *
			float3x4::RotateY(pileStackRotations.at(index));
}

template <typename T>
struct ArrayRef {
	T* p;
	size_t n;

	ArrayRef() : p(nullptr), n(0) {}
	ArrayRef(T* p, size_t n) : p(p), n(n) {}

	T* data() const { return p; }
	T* begin() const { return p; }
	T* end() const { return p+n; }

	size_t count() const { return n; }
	size_t byteSize() const { return n*sizeof(T); }
};

struct Mesh : ace::Noncopyable {
	struct StreamInfo {
		std::string semantic;
		int bufferIndex;
		GLuint elementCount;
		GLenum elementType;
		GLsizeiptr offset;
		GLboolean normalized;
		GLuint stride;
	};

	static StreamInfo MakeStreamInfo(std::string semantic,
									 int bufferIndex,
									 GLuint elementCount,
									 GLenum elementType,
									 GLsizeiptr offset,
									 GLboolean normalized,
									 GLuint stride)
	{
		StreamInfo ret = { semantic, bufferIndex, elementCount, elementType, offset, normalized, stride };
		return ret;
	}

	~Mesh() {
		glDeleteBuffers(1, &indexBuffer);
		glDeleteBuffers(buffers.size(), buffers.data());
	}

	static Mesh* LoadFromTPL(std::function<void (tpl_node*)> loader);
	static Mesh* LoadFromMemory(char const* data, size_t size);
	static Mesh* LoadFromStream(std::istream& stream);
	static Mesh* LoadFromFile(std::string filename);

	static Mesh* PlaneFromBasis(float3 T, float3 B, float extentT, float extentB, float scaleU, float scaleV);

	GLuint IndexBuffer() const { return indexBuffer; }
	GLuint Buffer(int x) const { return buffers.at(x); }
	GLuint IndexCount() const { return indexCount; }
	GLenum IndexType() const { return indexType; }
	GLvoid* IndexOffset() const { return indexOffset; }
	std::vector<StreamInfo> StreamInfos() const { return streams; }

	Mesh(ArrayRef<char> indices, ArrayRef<char> vertices, ArrayRef<Mesh::StreamInfo> streamInfos, size_t ibStride);

private:
	GLuint indexBuffer;
	std::vector<GLuint> buffers;
	GLuint indexCount;
	GLenum indexType;
	GLvoid* indexOffset;

	std::vector<StreamInfo> streams;

};

struct StreamHelperI {
	std::istream& is;
	explicit StreamHelperI(std::istream& is) : is(is) {}

	template <typename T>
	StreamHelperI& Read(T& t) {
		is.read((char*)&t, sizeof(T));
		return *this;
	}

	StreamHelperI& ReadString8(std::string& out) {
		uint8_t n = 0;
		Read(n);
		std::vector<char> buf(n);
		is.read(buf.data(), buf.size());
		out.assign(buf.begin(), buf.end());
		return *this;
	}
};

template <typename POD>
bool SlurpFile(std::istream& is, std::vector<POD>& out) {
	auto base = is.tellg();
	is.seekg(0, std::ios::end);
	auto length = is.tellg() - base;
	is.seekg(base, std::ios::beg);
	auto podCount = length / sizeof(POD);
	if (length % sizeof(POD) != 0)
		++podCount;
	out.resize(podCount);
	is.read(reinterpret_cast<char*>(out.data()), length);
	return true;
}

template <typename POD>
bool SlurpFile(std::string filename, std::vector<POD>& out) {
	std::ifstream is(filename.c_str(), std::ios::binary);
	if (!is) {
		out.resize(0);
		return false;
	}
	return SlurpFile(is, out);
}

unsigned PossibleMipCount(unsigned width, unsigned height) {
	int n = 1;
	while ((width % 2) == 0 && (height % 2) == 0) {
		width /= 2;
		height /= 2;
		++n;
	}
	return n;
}

struct ImageView {
	uint8_t* p;
	int nRows;
	int nCols;
	int nComp;

	uint8_t& Index(int row, int col, int comp) { return p[comp + col*nComp + row*nCols*nComp]; }
};

GLuint LoadTexture(std::string filename, int numComponents, int numMips = 1) {
	int width = 0, height = 0;
	auto actualComponents = 0;
	auto* data = stbi_load(filename.c_str(), &width, &height, &actualComponents, numComponents);
	if (!data)
		return 0;
	GLuint texId = 0;
	glGenTextures(1, &texId);
	glBindTexture(GL_TEXTURE_2D, texId);
	GLuint internalFormat = 0;
	GLuint format = 0;
	GLuint type = GL_UNSIGNED_BYTE;
	switch (numComponents) {
	case 1: internalFormat = GL_R8;    format = GL_RED;  break;
	case 2: internalFormat = GL_RG8;   format = GL_RG;   break;
	case 3: internalFormat = GL_RGB8;  format = GL_RGB;  break;
	case 4: internalFormat = GL_RGBA8; format = GL_RGBA; break;
	}
	int maxMips = PossibleMipCount(width, height);
	if (numMips == 0) {
		numMips = maxMips;
	}
	else {
		numMips = std::min<int>(maxMips, numMips);
	}
	for (int level = 0; level < numMips; ++level) {
		glTexImage2D(GL_TEXTURE_2D, level, internalFormat, width, height, 0, format, type, data);
		ImageView parent = { data, height, width, numComponents };
		ImageView child = { data, height/2, width/2, numComponents };
		width /= 2;
		height /= 2;
		if (level < numMips-1) {
			for (int row = 0; row < height; ++row) {
				for (int col = 0; col < width; ++col) {
					for (int comp = 0; comp < numComponents; ++comp) {
						float s[] = {
							parent.Index(row*2,   col*2,   comp) / 255.0f,
							parent.Index(row*2,   col*2+1, comp) / 255.0f,
							parent.Index(row*2+1, col*2,   comp) / 255.0f,
							parent.Index(row*2+1, col*2+1, comp) / 255.0f
						};
						float avg = (s[0]*s[0] + s[1]*s[1] + s[2]*s[2] + s[3]*s[3]) / 4.0f;
						child.Index(row, col, comp) = (uint8_t)(255.0f * Clamp01(sqrt(avg)));
					}
				}
			}
		}
	}
	stbi_image_free(data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, numMips-1);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	return texId;
}

Mesh::Mesh(ArrayRef<char> indices, ArrayRef<char> vertices,
		   ArrayRef<Mesh::StreamInfo> streamInfos, size_t ibStride)
{
	GLuint ib, vb;
	glGenBuffers(1, &ib);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.byteSize(), indices.data(), GL_STATIC_DRAW);

	glGenBuffers(1, &vb);
	glBindBuffer(GL_ARRAY_BUFFER, vb);
	glBufferData(GL_ARRAY_BUFFER, vertices.byteSize(), vertices.data(), GL_STATIC_DRAW);

	this->indexOffset = 0;
	GLenum ibTypes[] = { 0, GL_UNSIGNED_BYTE, GL_UNSIGNED_SHORT, 0, GL_UNSIGNED_INT };
	this->indexBuffer = ib;
	this->indexType = ibTypes[ibStride];
	this->indexCount = indices.byteSize() / ibStride;
	this->streams = std::vector<Mesh::StreamInfo>(streamInfos.begin(), streamInfos.end());
	this->buffers.push_back(vb);
};

Mesh* Mesh::LoadFromTPL(std::function<void (tpl_node*)> loader) {
	acemesh::Header header;
	acemesh::GeometryInfo geometryInfo;
	acemesh::StreamInfo streamInfo;
	tpl_bin indicesBin;
	tpl_bin verticesBin;
	auto tn = tpl_map(acemesh::FormatString(), &header, &streamInfo, &geometryInfo, &indicesBin, &verticesBin);
	loader(tn);
	tpl_unpack(tn, 0);

	std::vector<Mesh::StreamInfo> sis;
	while (tpl_unpack(tn, 1) > 0) {
		Mesh::StreamInfo si;
		si.bufferIndex = 0;
		si.elementCount = streamInfo.size / sizeof(float);
		si.elementType = GL_FLOAT;
		si.normalized = GL_FALSE;
		si.offset = streamInfo.offset;
		si.semantic = streamInfo.semantic;
		free((void*)streamInfo.semantic);
		si.stride = header.vertexStride;
		sis.push_back(si);
	}

	auto ret = new Mesh(ArrayRef<char>((char*)indicesBin.addr, indicesBin.sz),
						ArrayRef<char>((char*)verticesBin.addr, verticesBin.sz),
						ArrayRef<Mesh::StreamInfo>(sis.data(), sis.size()),
						header.indexStride);
	tpl_free(tn);
	return ret;
}

Mesh* Mesh::LoadFromMemory(const char* data, size_t size) {
	return LoadFromTPL([&](tpl_node* tn) {
		tpl_load(tn, TPL_MEM, data, size);
	});
}

Mesh* Mesh::LoadFromStream(std::istream& is) {
	if (!is)
		return nullptr;
	std::vector<char> buf;
	if (!SlurpFile(is, buf))
		return nullptr;
	return LoadFromMemory(buf.data(), buf.size());
}

Mesh* Mesh::LoadFromFile(std::string filename) {
	return LoadFromTPL([&](tpl_node* tn) {
		tpl_load(tn, TPL_FILE, filename.c_str());
	});
}

Mesh* Mesh::PlaneFromBasis(float3 T, float3 B, float extentT, float extentB, float scaleU, float scaleV) {
	T *= extentT/2;
	B *= extentB/2;
	std::array<float3, 4> positions = {
		-T + -B,
		+T + -B,
		+T + +B,
		-T + +B
	};

	float U = extentT*scaleU;
	float V = extentB*scaleV;
	std::array<float2, 4> texcoords = {
		float2(0.0f, 0.0f),
		float2(0.0f, V),
		float2(U,    V),
		float2(U,    0.0f)
	};

	float3 N = Cross(T, B).Normalized();
	std::array<float3, 4> normals = {
		N, N, N, N
	};

	auto const VertexSize = sizeof(acemesh::Vertex);
	std::vector<char> vbData(4 * VertexSize);
	{
		auto p = vbData.data();
		for (int i = 0; i < 4; ++i) {
			acemesh::Vertex v;
			v.v = positions[i];
			v.t = float3(texcoords[i], 0.0f);
			v.n = normals[i];
			memcpy(p, &v, VertexSize); p += VertexSize;
		}
	}
	std::vector<char> ibData(6 * sizeof(uint8_t));
	uint8_t indices[] = { 0, 1, 2, 0, 2, 3 };
	memcpy(ibData.data(), &indices, ibData.size());

	Mesh::StreamInfo sis[3] = {
		Mesh::MakeStreamInfo("POSITION", 0, 3, GL_FLOAT, 0,  GL_FALSE, sizeof(acemesh::Vertex)),
		Mesh::MakeStreamInfo("TEXCOORD", 0, 3, GL_FLOAT, 12, GL_FALSE, sizeof(acemesh::Vertex)),
		Mesh::MakeStreamInfo("NORMAL",   0, 3, GL_FLOAT, 24, GL_FALSE, sizeof(acemesh::Vertex))
	};

	return new Mesh(ArrayRef<char>(ibData.data(), ibData.size()),
					ArrayRef<char>(vbData.data(), vbData.size()),
					ArrayRef<Mesh::StreamInfo>(sis, 3),
					sizeof(uint8_t));
}

struct Material {
	GLuint ViewLocation() const { return uViewLoc; }
	GLuint ProjLocation() const { return uProjLoc; }
	GLuint WorldLocation() const { return uWorldLoc; }
	GLuint LightPosLocation() const { return uLightPosLoc; }

	GLuint AttribLocation(std::string semantic) {
		if (semantic == "POSITION") return aPositionLoc;
		if (semantic == "TEXCOORD") return aTexcoordLoc;
		if (semantic == "NORMAL") return aNormalLoc;
		return 0;
	}

	GLuint Program() const { return program.id; }

	gl::ProgramInfo ProgramInfo() const { return programInfo; }

	static Material* Load(std::string vsFilename, std::string fsFilename) {
		std::vector<char> source;
		GLuint vs = glCreateShader(GL_VERTEX_SHADER);
		GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);

		if (!SlurpFile(vsFilename, source)) {
			fprintf(stderr, "Material::Load could not slurp vertex shader file \"%s\".\n",
					vsFilename.c_str());
			return nullptr;
		}
		gl::ShaderSource(vs, source);

		if (!SlurpFile(fsFilename, source)) {
			fprintf(stderr, "Material::Load could not slurp fragment shader file \"%s\".\n",
					fsFilename.c_str());
			return nullptr;
		}
		gl::ShaderSource(fs, source);

		auto vsResult = gl::CompileShader(vs, gl::CompileFlagStoreLog);
		auto fsResult = gl::CompileShader(fs, gl::CompileFlagStoreLog);
		std::cerr << gl::DescribeResult("VS", vsResult) << "\n"
				  << gl::DescribeResult("FS", fsResult) << std::endl;

		if (!vsResult.success || !fsResult.success) {
			fprintf(stderr, "Material::Load failed compiling vertex/fragment shaders.\n");
			return nullptr;
		}
		GLuint program = glCreateProgram();
		glAttachShader(program, vs);
		glAttachShader(program, fs);
		auto progResult = gl::LinkProgram(program, gl::LinkFlagStoreLog);
		std::cerr << gl::DescribeResult(progResult) << std::endl;

		glDeleteShader(vs);
		glDeleteShader(fs);
		if (!progResult.success) {
			fprintf(stderr, "Material::Load failed linking shader program.\n");
			return nullptr;
		}

		auto programInfo = gl::QueryProgramInfo(program);

		Material* ret = new Material();
		ret->program.id = program;
		ret->programInfo = programInfo;
		ret->uViewLoc = programInfo.uniforms["in_view"].location;
		ret->uProjLoc = programInfo.uniforms["in_projection"].location;
		ret->uWorldLoc = programInfo.uniforms["in_world"].location;
		ret->uLightPosLoc = programInfo.uniforms["in_lightPos"].location;
		ret->aPositionLoc = programInfo.attribs["in_position"].location;
		ret->aTexcoordLoc = programInfo.attribs["in_texcoord"].location;
		ret->aNormalLoc = programInfo.attribs["in_normal"].location;
		return ret;
	}

private:
	gl::Program program;
	GLuint uViewLoc, uProjLoc, uWorldLoc, uLightPosLoc;
	GLuint aPositionLoc, aTexcoordLoc, aNormalLoc;

	gl::ProgramInfo programInfo;
};

struct MeshInstance {
	float3x4 transform;
	Mesh* mesh;
	Material* material;
	std::map<std::string, GLuint> textures;
};

std::vector<MeshInstance*> sceneInstances;

struct DrawVisitor {
	Camera camera;

	void DrawMeshInstance(MeshInstance const* instance) {
		auto* material = instance->material;
		auto* mesh = instance->mesh;
		float4x4 xform = instance->transform;
		glUseProgram(material->Program());
		glUniformMatrix4fv(material->ViewLocation(), 1, GL_FALSE, camera.view.Transposed().ptr());
		glUniformMatrix4fv(material->ProjLocation(), 1, GL_FALSE, camera.projection.Transposed().ptr());
		glUniformMatrix4fv(material->WorldLocation(), 1, GL_FALSE, xform.Transposed().ptr());
		auto progInfo = material->ProgramInfo();
		int nextTexture = 0;
		for (auto& tp : instance->textures) {
			auto locI = progInfo.uniforms.find(tp.first);
			if (locI == progInfo.uniforms.end())
				continue;
			glActiveTexture(GL_TEXTURE0 + nextTexture);
			glBindTexture(GL_TEXTURE_2D, tp.second);
			glUniform1i(locI->second.location, nextTexture);
			++nextTexture;
		}
		double now = glfwGetTime();
		float3 lightWorld;
		lightWorld = float3(sinf(now), 50.0f, cosf(now));
		lightWorld = float3(0.0f, 50.0f, 0.0f);
		//lightWorld = float3(0.0f, 50.0f + sin(now) * 25.0f, 0.0f);
		float3 lightView = camera.view.MulPos(lightWorld);
		glUniform3fv(material->LightPosLocation(), 1, lightView.ptr());
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->IndexBuffer());
		for (auto& stream : mesh->StreamInfos()) {
			auto bufferId = mesh->Buffer(stream.bufferIndex);
			glBindBuffer(GL_ARRAY_BUFFER, bufferId);
			auto loc = material->AttribLocation(stream.semantic);
			glVertexAttribPointer(loc, stream.elementCount, stream.elementType,
								  stream.normalized, stream.stride, (GLvoid* const)stream.offset);
			glEnableVertexAttribArray(loc);
		}
		glDrawElements(GL_TRIANGLES, mesh->IndexCount(), mesh->IndexType(), mesh->IndexOffset());
	}
};

struct TextureCache {
	GLuint Load(std::string filename, int components) {
		auto key = std::make_pair(filename, components);
		auto I = textures.find(key);
		if (I != textures.end())
			return I->second;
		auto id = LoadTexture(filename, components);
		textures[key] = id;
		return id;
	}

	std::map<std::pair<std::string, int>, GLuint> textures;
};

static TextureCache* textureCache = nullptr;

static Mesh* cardMesh = nullptr;
static Material* cardMaterial = nullptr;

struct CardInstanceFactory {
	CardInstanceFactory() {
		char const* SuitLetters = "HDSC";
		surfaceTex = textureCache->Load("data/074.jpg", 3);
		for (int value = 1; value <= 13; ++value) {
			for (int suit = 0; suit < 4; ++suit) {
				ace::Card c;
				c.suit = (ace::Card::Suit)suit;
				c.value = value;
				char filename[40] = {};
				sprintf(filename, "data/card%c%d.png", SuitLetters[c.suit], c.value);
				auto texId = textureCache->Load(filename, 4);
				glyphTextures[c] = texId;
			}
		}
	}

	MeshInstance CreateBlankCard() {
		MeshInstance ret;
		ret.material = cardMaterial;
		ret.mesh = cardMesh;
		ret.transform =
				float3x4::identity;
		ret.textures["in_surface"] = surfaceTex;
		return ret;
	}

	MeshInstance CreateCard(ace::Card c) {
		MeshInstance ret = CreateBlankCard();
		ret.textures["in_glyph"] = glyphTextures[c];
		return ret;
	}

	GLuint surfaceTex;
	std::map<ace::Card, GLuint, ace::LexicalCardComparer> glyphTextures;
};

static CardInstanceFactory* cardInstanceFactory = nullptr;

void Draw(Camera camera, ace::Game const& game) {
	DrawVisitor v;
	v.camera = camera;
	for (auto* instance : sceneInstances) {
		v.DrawMeshInstance(instance);
	}

	for (int lane = 0; lane < 4; ++lane) {
		int n = game.CardCountInLane(lane);
		for (int slot = 0; slot < n; ++slot) {
			ace::Card c = game.CardInLane(lane, slot);
			auto instance = cardInstanceFactory->CreateCard(c);
			instance.transform = LaneLocation(lane, slot);
			v.DrawMeshInstance(&instance);
		}
	}
	int nDeck = game.CardCountInDeck();
	auto deckCard = cardInstanceFactory->CreateBlankCard();
	for (int i = 0; i < nDeck; ++i) {
		deckCard.transform = DeckLocation(i);
		v.DrawMeshInstance(&deckCard);
	}
	int nPile = game.CardCountInPile();
	for (int i = 0; i < nPile; ++i) {
		ace::Card c = game.CardInPile(i);
		auto instance = cardInstanceFactory->CreateCard(c);
		instance.transform = PileLocation(i);
		v.DrawMeshInstance(&instance);
	}
}

enum class InputDesireTag : uint8_t {
	LaneClick, DealCards, DiscardCard, UIClick
};

struct InputDesireEvent {
	InputDesireTag tag;
	union Events {
		struct LaneClick {
			int lane;
		} laneClick;
		struct DealCards {
		} dealCards;
		struct DiscardCard {
			int lane;
		} discardCard;
		struct UIClick {
			uint32_t option;
		} uiClick;
	} event;
};

static Camera cam;
static std::deque<InputDesireEvent> inputDesires;

static float3 eye = float3(0.0f, 50.0f, 40.0f); // in world space

void OnKey(GLFWwindow* window, int key, int scancode, int action, int mods) {
	auto pressed = [&](int k) { return k == key && action == GLFW_PRESS; };
	auto released = [&](int k) { return k == key && action == GLFW_RELEASE; };
	if (pressed(GLFW_KEY_ESCAPE)) {
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
	if (released(GLFW_KEY_SPACE)) {
		InputDesireEvent e;
		e.tag = InputDesireTag::DealCards;
		inputDesires.push_back(e);
	}
	if (key >= GLFW_KEY_1 && key <= GLFW_KEY_4 && action == GLFW_RELEASE) {
		InputDesireEvent e;
		e.tag = InputDesireTag::DiscardCard;
		e.event.discardCard.lane = key - GLFW_KEY_1;
		inputDesires.push_back(e);
	}
	if (pressed(GLFW_KEY_LEFT)) {
		eye.x -= 10.0f;
	}
	if (pressed(GLFW_KEY_RIGHT)) {
		eye.x += 10.0f;
	}
	if (pressed(GLFW_KEY_UP)) {
		eye.z -= 10.0f;
	}
	if (pressed(GLFW_KEY_DOWN)) {
		eye.z += 10.0f;
	}
	if (pressed(GLFW_KEY_PAGE_DOWN)) {
		eye.y -= 10.0f;
	}
	if (pressed(GLFW_KEY_PAGE_UP)) {
		eye.y += 10.0f;
	}
}

void OnMouseButton(GLFWwindow* window, int button, int action, int mods) {
	double x, y;
	glfwGetCursorPos(window, &x, &y);
	if (button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS) {
		GLint viewport[4] = {};
		glGetIntegerv(GL_VIEWPORT, viewport);
		Rect vp;
		vp.left = viewport[0];
		vp.top = viewport[1];
		vp.right = vp.left + viewport[2];
		vp.bottom = vp.top + viewport[3];
		float3 zNear = gl::UnProject(float3(x, y, 0.0f), cam.view, cam.projection, vp);
		float3 zFar = gl::UnProject(float3(x, y, 1.0f), cam.view, cam.projection, vp);
		fprintf(stderr, "Click: %f %f %f -> %f %f %f\n",
				zNear.x, zNear.y, zNear.z,
				zFar.x, zFar.y, zFar.z);
		LineSegment line(zNear, zFar);
		bool hitLane = false;
		int bestLane;
		float dBest = 1.0;
		for (int lane = 0; lane < 4; ++lane) {
			auto bounds = LaneBounds(lane);
			float dNear = 0, dFar = line.Length();
			if (line.Intersects(bounds, dNear, dFar)) {
				if (hitLane && dNear > dBest) {
					continue;
				}
				hitLane = true;
				dBest = dNear;
				bestLane = lane;
			}
		}
		if (hitLane) {
			fprintf(stderr, "Hit lane #%d, %f\n", bestLane, dBest);
		}
		float distanceToOrigin = line.Distance(float3(0.0f, 0.0f, 0.0f));
		fprintf(stderr, "Distance to origin: %f\n", distanceToOrigin);
	}
}


void ProcessInputDesires(ace::Game* game)
{
	for (auto d : inputDesires) {
		switch (d.tag) {
		case InputDesireTag::DealCards:
			if (game->CanDealCards())
				game->DealCards();
			break;
		case InputDesireTag::DiscardCard: {
			auto lane = d.event.discardCard.lane;
			if (game->CanDiscardCard(lane))
				game->DiscardCard(lane);
			break;
		}
		default:
			break;
		}
	}
	inputDesires.clear();
}

MeshInstance* MakeTable()
{
	auto material = Material::Load("data/prop.vert", "data/prop.frag");
	auto* tableMesh = Mesh::LoadFromFile("data/table.acemesh");
	GLuint tableTex = textureCache->Load("data/table.png", 3);

	auto* tableInstance = new MeshInstance();
	tableInstance->material = material;
	tableInstance->mesh = tableMesh;
	tableInstance->transform =
			float3x4::Translate(0.0f, -0.4f, 0.0f) *
			float3x4::Scale(10.0f, 10.0f, 10.0f);
	tableInstance->textures["in_surface"] = tableTex;

	return tableInstance;
}

MeshInstance* MakeFloor()
{
	auto material = Material::Load("data/prop.vert", "data/prop.frag");
	auto* floorMesh = Mesh::PlaneFromBasis(float3::unitX, -float3::unitZ,
										   1000.0f, 1000.0f, 1.0f/10.0f, 1.0f/10.0f);
	GLuint floorTex = textureCache->Load("data/038.jpg", 3);
	auto* floorInstance = new MeshInstance();
	floorInstance->material = material;
	floorInstance->mesh = floorMesh;
	floorInstance->transform = float3x4::Translate(0.0f, -40.0f, 0.0f);
	floorInstance->textures["in_surface"] = floorTex;

	return floorInstance;
}

int main() {
	int w = 1280, h = 720;
	glfwInit();
	glfwWindowHint(GLFW_DEPTH_BITS, 32);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	GLFWwindow* window = glfwCreateWindow(1280, 720, "Aces Up Simulator 2014", nullptr, nullptr);
	glfwSetKeyCallback(window, &OnKey);
	glfwSetMouseButtonCallback(window, &OnMouseButton);
	glfwMakeContextCurrent(window);
	{
		fprintf(stderr, "OpenGL context version %d.%d.%d\n",
			glfwGetWindowAttrib(window, GLFW_CONTEXT_VERSION_MAJOR),
			glfwGetWindowAttrib(window, GLFW_CONTEXT_VERSION_MINOR),
			glfwGetWindowAttrib(window, GLFW_CONTEXT_REVISION));
		fflush(stderr);
	}
	glfwSwapInterval(1);
	glxwInit();

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	GLuint defaultVAO;
	glGenVertexArrays(1, &defaultVAO);
	glBindVertexArray(defaultVAO);

	textureCache = new TextureCache();
	cardInstanceFactory = new CardInstanceFactory();

	cardMaterial = Material::Load("data/card.vert", "data/card.frag");
	cardMesh = Mesh::LoadFromFile("data/card.acemesh");

	GLuint emptyGlyph = textureCache->Load("data/1x1trans.png", 4);

	auto* tableInstance = MakeTable();
	sceneInstances.push_back(tableInstance);
	auto* floorInstance = MakeFloor();
	sceneInstances.push_back(floorInstance);

	ace::Game* game = new ace::Game();

	struct CameraBuilder {
		float3 at;
		float3 localForward;
		float3 localUp;
		float3 worldUp;

		float aspect;
		float zNear;
		float planeScale;

		CameraBuilder(float w, float h)
		: at(float3::zero)                                // in world space
		, localForward(-float3::unitZ)                    // in view space
		, localUp(float3::unitY)                          // in view space
		, worldUp(float3(0.0f, 1.0f, -1.0f).Normalized()) // in world space
		, aspect(w/h)
		, zNear(0.1f)
		, planeScale(1.0f*zNear) {}

		float4x4 GetProjectionMatrix() {
			return float4x4::OpenGLPerspProjRH(zNear, 1000.0f, aspect*planeScale, planeScale);
		}

		float4x4 GetViewMatrix() {
			float4x4 m = float4x4::LookAt(eye, at, localForward, localUp, worldUp);
			m.InverseOrthonormal();
			return m;
		}
	};

	CameraBuilder cameraBuilder((float)w, (float)h);
	cam.projection = cameraBuilder.GetProjectionMatrix();

	glClearColor(0.1f, 0.2f, 0.3f, 1.0f);
	while (!glfwWindowShouldClose(window)) {
		double now = glfwGetTime();
		(void)now;
		cam.view = cameraBuilder.GetViewMatrix();

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		ProcessInputDesires(game);
		Draw(cam, *game);

		glfwPollEvents();
		glfwSwapBuffers(window);
	}

	delete game;
	glfwTerminate();
}
